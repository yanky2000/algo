const prices1 = [7,1,5,3,6,4]; // 5
const prices = [2,1,2,0,1]; // 1
const prices2 = [6,5,4,3,1]; // 0
const prices3 = [2,1,2,1,0,1,2];

// exercise 121. Not optimal solution!!!

/* 
Say you have an array for which the ith element is the price of a given stock on day i.
If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.
Note that you cannot sell a stock before you buy one.
 */

var maxProfit = function(prices) {
    let minPrice;
    let maxProfit = 0;
    
    for( let i = 0; i < prices.length; i++) {

        // register minPrice
        if(prices[i] < prices[i+1] ) {
            minPrice= minPrice !== undefined ? Math.min(prices[i], minPrice) : prices[i];
        }
        
        if(minPrice !== undefined) {
            // calc profit only if we bought anything
            maxProfit = Math.max(maxProfit, prices[i] - minPrice)
        }
    }

    return maxProfit
};

console.log(maxProfit(prices1));
console.log(maxProfit(prices));
console.log(maxProfit(prices2));
console.log(maxProfit(prices3));
