// const string = "[()]";
const string = 
"(]"
function isValidString(string) {
    const stack = [];
    // const openBrackets = bracketsMap.keys();
    const bracketsMap = { "(": ")", "[": "]", "{": "}" };

    function isClosingBracket(c) {
        const closedBrackets = Object.values(bracketsMap);
        return closedBrackets.includes(c);
    }

    for (let char of string) {
        // skip space character
        if (char.length) {
            if (!stack.length) {
                // closing bracket comes first is an error
                if (isClosingBracket(char)) return false;

                stack.push(char);
            } else {
                const lastBracket = stack[stack.length - 1];

                // char is closing bracket
                if (isClosingBracket(char)) {
                    stack.pop();
                    if (bracketsMap[lastBracket] !== char) return false;
                    continue;
                }

                // char is opening bracket or empty string
                stack.push(char);
            }
        }
    }
    return stack.length ? false : true;
}
