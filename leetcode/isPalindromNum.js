const num = 121;

function isPalindrom(num) {
    const n = num.toString();
    if (!n.length) return true;

    let start = 0;
    let end = n.length - 1;

    while (start < end) {
        if (n[start] !== n[end]) return false;
        start++;
        end--;
    }

    return true;
}


