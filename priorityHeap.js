class Node {
    constructor(val, priority) {
        this.values = val;
        this.priority = priority;
    }
}
class PriorityHeap {
    constructor() {
        this.values = [];
        // this.values = [25, 19, 13, 10];
    }
    enqueue(val, priority) {
        this.values.push(new Node(val, priority));
        this.bubbleUp();
    }
    bubbleUp() {
        let idx = this.values.length - 1;
        const element = this.values[idx];
        while (idx > 0) {
            const parentIdx = Math.floor((idx - 1) / 2);
            let parentEl = this.values[parentIdx];
            if (parentEl.priority <= element.priority) {
                break;
            }
            this.values[parentIdx] = element;
            this.values[idx] = parentEl;
            idx = parentIdx;
        }
    }
    extractMax() {
        const max = this.values[0];
        const end = this.values.pop();
        if (this.values.length > 0) {
            this.values[0] = end;
            this.sinkDown();
        }
        return max;
    }
    sinkDown() {
        let idx = 0;
        let element = this.values[0];

        while (true) {
            let swap = null;
            // find left & right children
            let leftChildIdx = idx * 2 + 1;
            let rightChildIdx = idx * 2 + 2;
            // handle left child
            // check if child idx is in bound
            if (leftChildIdx < this.values.length) {
                // if child greater and root swap. swap with greatest child
                if (element.priority > this.values[leftChildIdx].priority) {
                    swap = leftChildIdx;
                }
            }

            // handle right child
            // idx is in bound
            if (rightChildIdx < this.values.length) {
                if (
                    (swap === null && element.priority > this.values[rightChildIdx].priority) ||
                    (swap !== null &&
                        this.values[leftChildIdx].priority > this.values[rightChildIdx.priority])
                ) {
                    swap = rightChildIdx;
                }
                // continue to compare up untill no swap is needed
            }
            if (swap === null) break;
            this.values[idx] = this.values[swap];
            this.values[swap] = element;
            idx = swap;
        }
    }

}
const heap = new PriorityHeap()
heap.enqueue('i12', 2)
heap.enqueue('i13', 1)
heap.enqueue('i14', 3)
heap.enqueue('i15', 1)
heap.enqueue('i17', 1)
// heap
// const heap = new Heap();
// // heap.insert(4);
// heap.insert(44);
// heap.insert(11);
// heap.insert(12);
const s = heap.extractMax();
const s2 = heap.extractMax();
// // s
s2
heap;
// //      44
// // 25      13
// // 19 12   10 8
// // 14 11 3
