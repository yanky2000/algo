// class Node {
//   constructor(val) {
//     this.value = val;
//     this.next = null;
//   }
// }

// class Queue {
//   constructor() {
//     this.first = null;
//     this.last = null;
//     this.size = 0;
//   }
//   enqueue(val) {
//     const newNode = new Node(val);
//     if (!this.first) {
//       this.first = newNode;
//       this.last = newNode;
//     } else {
//       this.last.next = newNode;
//       this.last = newNode;
//     }
//     return ++this.size;
//   }
//   dequeue() {
//     if (!this.first) return null;
//     const firstNode = this.first;
//     if (this.first == this.last) {
//       this.last = null;
//     }
//     this.first = this.first.next;
//     this.size--;
//     return firstNode;
//   }
// }
// const q = new Queue();
// q.enqueue("helllo");
// q.dequeue();
// q;

// const ar = []
// const s = ar.push(1)

// BST
class Node {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

class BST {
    constructor() {
        this.root = null;
    }
    insert(val) {
        const newNode = new Node(val);
        // if bst is empty
        if (this.root === null) {
            this.root = newNode;
            return this;
        } else {
            let current = this.root;
            while (true) {
                if (val < current.value) {
                    // add new left
                    if (current.left === null) {
                        current.left = newNode;
                        return this;
                    } else {
                        // mode one level down
                        current = current.left;
                    }
                } else if (val > current.value) {
                    if (current.right === null) {
                        current.right = newNode;
                        return this;
                    } else {
                        current = current.right;
                    }
                }
            }
        }
    }
}

// insert
/* 1. if bst is empty make new root
2. create pointer
3. if val < pointer 
3.1 check if pointer.left exists
if not make pointer.left

if exist move pointer to while loop
*/

class CNode {
    constructor(val) {
        this.value = val;
        this.left = null;
        this.right = null;
    }
}

class BST2 {
    constructor() {
        this.root = null;
    }
    insert(value) {
        const newNode = new CNode(value);
        // bst empty
        if (!this.root) {
            this.root = newNode;
            return this;
        }

        let pointer = this.root;
        while (true) {
            if (value === pointer.value) return undefined;
            // handle left
            if (value < pointer.value) {
                if (!pointer.left) {
                    pointer.left = newNode;
                    return this;
                } else {
                    pointer = pointer.left;
                }
            } else if (value > pointer.value) {
                if (pointer.right) {
                    pointer = pointer.right;
                } else {
                    pointer.right = newNode;
                    return this;
                }
            }
        }
    }
    search(val) {
        if (!this.root) return false;

        let pointer = this.root;
        while (true) {
            if (val === pointer.value) return pointer.value;

            if (val < pointer.value) {
                if (!pointer.left) return false;
                pointer = pointer.left;
            }
            if (val > pointer.value) {
                if (!pointer.right) return false;
                pointer = pointer.right;
            }
        }
    }
    BFS() {
        const data = [];
        const queue = [];
        let node = this.root;
        queue.push(node);
        while (queue.length) {
            node = queue.shift();
            data.push(node.value);
            if (node.left) {
                queue.push(node.left);
            }
            if (node.right) {
                queue.push(node.right);
            }
        }
        return data;
    }
    DFS() {
        const data = [];

        function traverse(node) {
            data.push(node.value)
            if(node.left) { traverse(node.left)}
            if(node.right) { traverse(node.right)}
        }
        traverse(this.root)

        return data;
    }
}

const tree = new BST2();
tree.insert(23);
tree.insert(9);
tree.insert(29);
tree.insert(19);
tree.insert(2);
const s = tree.BFS();

const l = tree.DFS()
l