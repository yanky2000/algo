#Overview of data structures

# BST
Insertion O(log n)
Searching O(n) *not guaranteed!*

# Heaps
insertion O(log N)
delettion O(log N)
search O(N)

could be min or max heaps. 
extracting min (max) element is fast

### Usage
- usefu$l for priority queues (sorting nature)

### Key features: 
- each node could have maximun 2 children
- every parent larger (or less) than its children
- fills from left to right
- no relation between children
- can be represented by arrays

# Hash Tables
insert, delete, access O(1)
search O(n)