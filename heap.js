class Heap {
    constructor() {
        this.values = [25, 19, 13, 14, 3, 10, 8];
        // this.values = [25, 19, 13, 10];
    }
    insert(val) {
        this.values.push(val);
        this.bubbleUp();
    }
    bubbleUp() {
        let idx = this.values.length - 1;
        const elementVal = this.values[idx];
        while (idx > 0) {
            const parentIdx = Math.floor((idx - 1) / 2);
            let parentVal = this.values[parentIdx];
            if (parentVal >= elementVal) {
                break;
            }
            this.values[parentIdx] = elementVal;
            this.values[idx] = parentVal;
            idx = parentIdx;
        }
    }
    extractMax() {
        const max = this.values[0];
        const end = this.values.pop();
        if (this.values.length > 0) {
            this.values[0] = end;
            this.sinkDown();
        }
        return max;
    }
    sinkDown() {
        let idx = 0;
        let element = this.values[0];

        while (true) {
            let swap = null;
            // find left & right children
            let leftChildIdx = idx * 2 + 1;
            let rightChildIdx = idx * 2 + 2;
            // handle left child
            // check if child idx is in bound
            if (leftChildIdx < this.values.length) {
                // if child greater and root swap. swap with greatest child
                if (element < this.values[leftChildIdx]) {
                    swap = leftChildIdx;
                }
            }

            // handle right child
            // idx is in bound
            if (rightChildIdx < this.values.length) {
                if (
                    (swap === null && element < this.values[rightChildIdx]) ||
                    (swap !== null &&
                        this.values[leftChildIdx] < this.values[rightChildIdx])
                ) {
                    swap = rightChildIdx;
                }
                // continue to compare up untill no swap is needed
            }
            if (swap === null) break;
            this.values[idx] = this.values[swap];
            this.values[swap] = element;
            idx = swap;
        }
    }

}

// const heap = new Heap();
// // heap.insert(4);
// heap.insert(44);
// heap.insert(11);
// heap.insert(12);
// // const s = heap.extractMax();
// const s2 = heap.extractMax();
// // s
// s2
// heap;
// //      44
// // 25      13
// // 19 12   10 8
// // 14 11 3
